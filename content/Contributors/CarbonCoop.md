Title: Carbon Coop
Tags: United Kingdom
Slug: carboncoop
Date: 2023-09-11 11:00

<img src="../images/carboncoop.png" width="200" />

Carbon Co-op is an energy services and advocacy co-operative that helps people and communities to make the radical reductions in home carbon emissions necessary to avoid runaway climate change. Carbon Co-op undertakes energy systems projects, including REScoopVPP which includes software and hardware development.

### Projects

- [REScoopVPP](/rescoopvpp)
