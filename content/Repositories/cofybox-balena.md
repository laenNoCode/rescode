Title: COFYbox Balena
Date: 2023-09-11 11:00
Tags: python, balena, cofybox, docker
Slug: cofybox-balena
Contributors: EnergieID
    CarbonCoop
Project: REScoopVPP

### Repository location

[https://gitlab.com/rescoopvpp/cofybox-balena](https://gitlab.com/rescoopvpp/cofybox-balena)

### Description
All software running on the COFYbox is running in Docker style containers. All these containers are grouped and deployed via Balena. This repository contains the dockerfiles for all these containers, making it the main deployment package for COFYbox code.